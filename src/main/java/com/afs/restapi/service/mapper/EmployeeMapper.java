package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public EmployeeMapper() {
    }

    public static Employee toEntity(EmployeeRequest request) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(request,employee);
        return employee;
    }

    public static EmployeeResponse toResponse(Employee employee) {
        EmployeeResponse response = new EmployeeResponse();
        BeanUtils.copyProperties(employee,response);
        return response;
    }
}
