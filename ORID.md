# ORID

## O

​	Today's study focused on SQL and Spring Data JPA.In the section on SQL, I learned about basic concepts and operations such as how to create tables, insert data, query data, and update and delete data. In the section on Spring Data JPA, I learned how to make it simpler to operate on databases, including creating Entities, establishing mapping relationships, using the JpaRepository interface to simplify CRUD operations, and using the @Transient annotation to handle non-database fields. In this way, I gained a deeper understanding of the ORM framework and the conveniences that JPA offers us.

## R

full

## I

​	During my studies, I realized that SQL and Spring Data JPA are very important in real-world application development. Both direct database operations (e.g. SQL) and abstract operations through ORM frameworks (e.g. JPA) are the foundation of any complex application.

## D

​	I have decided to use my free time tomorrow to further study and practice more advanced features of SQL queries and Spring Data JPA. In addition, I also plan to write some small case studies for what I have learned today so that I can test my understanding and deepen my memory by practicing. I believe that in this way, I can better understand and master these important technical tools.